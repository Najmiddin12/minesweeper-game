import { Game } from './game';
import { GameState } from './types';

function createGameBoard(rows: number, cols: number, mines: number): Game {
  const game = new Game(rows, cols, mines);
  const gameContainer = document.getElementById('game-container')!;
  gameContainer.innerHTML = '';
  gameContainer.style.gridTemplateRows = `repeat(${rows}, 1fr)`;
  gameContainer.style.gridTemplateColumns = `repeat(${cols}, 1fr)`;

  game.getBoard().forEach((row, rowIndex) => {
    row.forEach((cell, colIndex) => {
      const cellElement = document.createElement('div');
      cellElement.className = 'cell';
      cellElement.addEventListener('click', () => {
        game.revealCell(rowIndex, colIndex);
        updateGameBoard(game);
      });
      cellElement.addEventListener('contextmenu', (e) => {
        e.preventDefault();
        game.toggleFlag(rowIndex, colIndex);
        updateGameBoard(game);
      });
      gameContainer.appendChild(cellElement);
    });
  });

  return game;
}

function updateGameBoard(game: Game): void {
  const gameContainer = document.getElementById('game-container')!;
  const cells = gameContainer.getElementsByClassName('cell');
  game.getBoard().forEach((row, rowIndex) => {
    row.forEach((cell, colIndex) => {
      const cellElement = cells[rowIndex * row.length + colIndex] as HTMLElement;
      cellElement.textContent = cell.isRevealed ? (cell.isMine ? '💣' : cell.adjacentMineCount.toString()) : '';
      cellElement.className = 'cell';
      if (cell.isRevealed) cellElement.classList.add('revealed');
      if (cell.isFlagged) cellElement.classList.add('flagged');
    });
  });

  const gameState = game.getGameState();
  if (gameState === GameState.Won) {
    alert('You won!');
  } else if (gameState === GameState.Lost) {
    alert('You lost!');
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const game = createGameBoard(10, 10, 5);
});
