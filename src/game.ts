import { GameState } from './types';
import { Cell } from './cell';

export class Game {
  private board: Cell[][];
  private state: GameState;
  private mineCount: number;

  constructor(rows: number, cols: number, mines: number) {
    this.board = this.createBoard(rows, cols);
    this.state = GameState.InProgress;
    this.mineCount = mines;
    this.placeMines(mines);
    this.calculateAdjacentMineCounts();
  }

  private createBoard(rows: number, cols: number): Cell[][] {
    return Array.from({ length: rows }, () => Array.from({ length: cols }, () => new Cell()));
  }

  private placeMines(mines: number): void {
    let placedMines = 0;
    while (placedMines < mines) {
      const row = Math.floor(Math.random() * this.board.length);
      const col = Math.floor(Math.random() * this.board[0].length);
      if (!this.board[row][col].isMine) {
        this.board[row][col].isMine = true;
        placedMines++;
      }
    }
  }

  private calculateAdjacentMineCounts(): void {
    for (let row = 0; row < this.board.length; row++) {
      for (let col = 0; col < this.board[0].length; col++) {
        if (!this.board[row][col].isMine) {
          this.board[row][col].adjacentMineCount = this.getAdjacentCells(row, col).filter(cell => cell.isMine).length;
        }
      }
    }
  }

  private getAdjacentCells(row: number, col: number): Cell[] {
    const neighbors: Cell[] = [];
    for (let r = row - 1; r <= row + 1; r++) {
      for (let c = col - 1; c <= col + 1; c++) {
        if (r >= 0 && r < this.board.length && c >= 0 && c < this.board[0].length && (r !== row || c !== col)) {
          neighbors.push(this.board[r][c]);
        }
      }
    }
    return neighbors;
  }

  public revealCell(row: number, col: number): void {
    const cell = this.board[row][col];
    if (cell.isRevealed || cell.isFlagged) return;

    cell.isRevealed = true;
    if (cell.isMine) {
      this.state = GameState.Lost;
      this.revealAllMines();
    } else if (cell.adjacentMineCount === 0) {
      this.getAdjacentCells(row, col).forEach(adjCell => this.revealCell(row, col));
    }

    if (this.checkWinCondition()) {
      this.state = GameState.Won;
    }
  }

  private revealAllMines(): void {
    this.board.flat().forEach(cell => {
      if (cell.isMine) cell.isRevealed = true;
    });
  }

  private checkWinCondition(): boolean {
    return this.board.flat().every(cell => cell.isRevealed || (cell.isMine && !cell.isRevealed));
  }

  public toggleFlag(row: number, col: number): void {
    const cell = this.board[row][col];
    if (!cell.isRevealed) {
      cell.isFlagged = !cell.isFlagged;
    }
  }

  public getGameState(): GameState {
    return this.state;
  }

  public getBoard(): Cell[][] {
    return this.board;
  }
}
