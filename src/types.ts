export enum GameState {
    InProgress,
    Won,
    Lost
}
  
export interface ICell {
    isMine: boolean;
    isRevealed: boolean;
    isFlagged: boolean;
    adjacentMineCount: number;
}
  