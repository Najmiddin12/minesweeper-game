import { Cell } from '../src/cell';

describe('Cell', () => {
  it('should create a cell with default properties', () => {
    const cell = new Cell();
    expect(cell.isMine).toBe(false);
    expect(cell.isRevealed).toBe(false);
    expect(cell.isFlagged).toBe(false);
    expect(cell.adjacentMineCount).toBe(0);
  });
});
