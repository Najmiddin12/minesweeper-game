import { Game } from '../src/game';
import { GameState } from '../src/types';

describe('Game', () => {
  it('should create a game with the correct size and mines', () => {
    const game = new Game(10, 10, 5);
    expect(game.getBoard().length).toBe(10);
    expect(game.getBoard()[0].length).toBe(10);

    let mineCount = 0;
    game.getBoard().forEach(row => row.forEach(cell => {
      if (cell.isMine) mineCount++;
    }));
    expect(mineCount).toBe(5);
  });

  it('should reveal a cell and not lose the game if it is not a mine', () => {
    const game = new Game(10, 10, 5);
    game.revealCell(0, 0);
    expect(game.getBoard()[0][0].isRevealed).toBe(true);
    expect(game.getGameState()).toBe(GameState.InProgress);
  });

  it('should reveal all mines and lose the game if a mine is clicked', () => {
    const game = new Game(10, 10, 5);
    let mineCell = game.getBoard().flat().find(cell => cell.isMine);
    if (mineCell) {
      const mineIndex = game.getBoard().flat().indexOf(mineCell);
      const row = Math.floor(mineIndex / 10);
      const col = mineIndex % 10;
      game.revealCell(row, col);
      expect(game.getGameState()).toBe(GameState.Lost);
      game.getBoard().forEach(row => row.forEach(cell => {
        if (cell.isMine) expect(cell.isRevealed).toBe(true);
      }));
    }
  });

  it('should allow flagging and unflagging of cells', () => {
    const game = new Game(10, 10, 5);
    game.toggleFlag(0, 0);
    expect(game.getBoard()[0][0].isFlagged).toBe(true);
    game.toggleFlag(0, 0);
    expect(game.getBoard()[0][0].isFlagged).toBe(false);
  });
});
